const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');
const scsslint = require('gulp-scss-lint');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const cache = require('gulp-cache');
const minifycss = require('gulp-minify-css');
const usemin = require('gulp-usemin');
const nunjucks = require('gulp-nunjucks-html');
const babel = require('gulp-babel');
const plumber = require('gulp-plumber');
const eslint = require('gulp-eslint');

// Development Tasks
// -----------------
gulp.task('nunjucks', function () {
  return gulp.src(['src/templates/**/*.html', '!src/templates/partials/**'])
    .pipe(plumber())
    .pipe(nunjucks({
      searchPaths: ['src/templates/']
    }))
    .pipe(gulp.dest('build/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('default', ['sass', 'nunjucks', 'scripts', 'scriptsLibs', 'icons', 'images', 'fonts'], function () {
  gulp.start('server');
  gulp.watch('src/templates/**/*.html', ['nunjucks']);
  gulp.watch('src/assets/scss/**/*.scss', ['sass']);
  gulp.watch('src/assets/js/**/*.js', ['scripts']);
  gulp.watch('src/assets/img/**/*', ['images']);
});

gulp.task('sass', function() {
  return gulp.src(['src/assets/scss/styles.scss', 'src/assets/scss/**/*.css'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 version'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/assets/css'))
    .pipe(browserSync.reload({stream: true }));
});

gulp.task('scripts', () =>
  gulp.src('src/assets/js/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({presets: ['env']}))
    .pipe(gulp.dest('build/assets/js'))
);

gulp.task('scriptsLibs', () =>
  gulp.src('src/assets/js/libs/*.js')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(gulp.dest('build/assets/js'))
);

gulp.task('server', function () {
  browserSync.init({
    server: {
      baseDir: 'build'
    }
  });
});

//Quality codes
gulp.task('scss-lint', function () {
  return gulp.src('/scss/*.scss')
    .pipe(scsslint())
    .pipe(gulp.dest('dist/css'));
});

gulp.task('build', ['clean'], function () {
  gulp.start('nunjucks', 'usemin', 'icons', 'imagesDeploy', 'fontsDeploy');
});

gulp.task('clean', function () {
  return gulp.src('dist')
    .pipe(clean());
});

// Optimizing CSS and JavaScript
gulp.task('usemin', ['sass'], function () {
  return gulp.src('build/*.html')
    .pipe(usemin({
      css: [minifycss],
      js: [uglify],
    }))
    .pipe(gulp.dest('dist/pages'));
});

// Optimizing Images
gulp.task('images', function () {
  return gulp.src('src/assets/img/**/*')
    .pipe(cache(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('build/assets/img'));
});

gulp.task('icons', function () {
  return gulp.src('src/assets/icons/**/*')
    .pipe(gulp.dest('build/assets/icons'));
});

// Copying fonts
gulp.task('fonts', function () {
  return gulp.src('src/assets/fonts/**/*')
    .pipe(gulp.dest('build/assets/fonts'))
});

gulp.task('imagesDeploy', function () {
  return gulp.src('build/assets/img/**/*')
    .pipe(cache(imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/assets/img'));
});

// Copying fonts
gulp.task('fontsDeploy', function () {
  return gulp.src('build/assets/fonts/**/*')
    .pipe(gulp.dest('dist/assets/fonts'))
});
